# About

Este Projeto faz parte do [Desafio Jusbrasil](https://gist.github.com/tarsisazevedo/966d469e8a80741334d3c4dce66cbea5) proposto para a vaga de [Backend Engineer | Data](https://github.com/jusbrasil/careers/blob/master/positions/Senior%20Backend%20Engineer%20-%20Data.md).


O projeto foi desenvolvido seguindo metodologias presentes no [The Twelve-Factor App](https://12factor.net/pt_br/).


Foi desenvolvida a API para consulta de dados de processos em todos os graus dos Tribunais de Justiça de Alagoas (TJAL) e do Mato Grosso do Sul (TJMS). Para obter os dados, são executados crawlers assincronamente.

A API tem a seguinte assinatura:

  * Input
    - Requisição POST. Segue exemplo:


    ```json
    {
      "numero_processo": "0708956-13.2012.8.02.0001"
    }
    ```

    ou

    `curl --location --request POST 'http://localhost:8000/api/v1/processos/' \
    --header 'Content-Type: application/json' \
    --data-raw '{
      "numero_processo": "0708956-13.2012.8.02.0001"
    }'`



  * Output
    - Conteúdo armazenado e disponível nos serviços da API. Através do GET é possível obter o processo. Quando processada, a consulta atualiza os dados do processo.


### Setup instructions


O projeto é executado dentro de um ambiente virtual com o auxílio de um gerenciador de pacotes. Como sugestão para resolver ambos os pontos o `pipenv` é recomendado. É preciso que seja usada a versão estável mais atual de `Python`.

Na raiz de cada projeto é possível encontrar o arquivo `local.env`. É necessário copiá-lo para os módulos `worker-consumer` e `rest-api`


```
cp rest-api/local.env rest-api/.env
cp worker-consumer/local.env worker-consumer/.env
```
e alterar para os valores locais correspondentes.


Todos os tópicos seguem o uso do prefixo definido CLOUDKARAFKA_TOPIC_PREFIX, definido na variável de ambiente no módulo rest-api. É necessário criar um tópico seguindo esse prefixo e com `justest` ao final do nome.


Instalado o `pipenv` execute cada um dos módulos `rest-api` e `worker-consumer`, respectivamente:


```
cd rest-api
pipenv shell
pipenv install
make migrate
make run
```



```
cd worker-consumer
pipenv shell
pipenv install
make run
```


Esses comandos deve instalar as dependências necessárias. ;) 


### REST API Documentation


Para mais informações sobre a API REST, os endpoints e assinaturas, é possível acessar a documentação provida pelo Django REST Swagger:


`http://localhost:8000/swagger/`



### System Architecure


![API's Architectural Design](docs/APIs_Architectural_Design.png)


Os principais componentes da arquitetura são:

* API REST (Django REST Framework) 
* Banco de dados (Sqlite, com facilidade de alteração para Postgres)
* Kafka (Single Consumer Group)
* Worker (API REST - Kafka - Crawler)

É importante saber sobre eles:

* API REST implementada em Django REST Framework: O framework foi escolhido por estar dentro da proposta de eficiência e simplicidade de implementação. "The web framework for perfectionists with deadlines."
  Com comunicação assíncrona por mensagem, é responsável por implementar os serviços disponíveis para o Processo Judicial (CRUD).
  - Com a biblioteca `django-simple-history` a API REST salva todo o histórico de mudanças do processo (cria e salva no BD), o que é importante para auditoria, rollback de mudanças e checagem de evolução do processo. 

* Banco de dados SQLite: escolhido pela simplicidade de executar o sistema. É o default do Django e pode ser alterado sem dificuldades para usar o Postgres, bastando alterar no settings do projeto.

* Kafka Pub/Sub pensado como um serviço de mensageria a suportar alta performance. Na seção Know Issues serão apresentadas melhorias a respeito do serviço para maior suporte e tolerância ao volume de dados/acessos.

* O Worker no projeto tem função fundamental: processar as consultas para extrair os dados dos processos. Ele pode executar em paralelo de maneira assíncrona e funções não bloqueantes. Ele é também construído para ter uma escalabilidade horizontal. Para evitar problemas de concorrência, é utilizado um lock distribuído com o Redis.


### Crawlers

Os Crawlers são executados no módulo Worker `worker-consumer`, foram implementados de maneira que cada crawler faça a busca de um Tribunal de Justiça, sem que seja necessário input extra do código referente ao estado. 

É suficiente enviar o número do Processo Judicial, o objeto gerado e salvo irá conter, caso existam, as informações dos processos no 1º Grau e no 2º, de maneira detalhada e dividida, retornando as informações de acordo com o solicitado no desafio.

* Classe
* Área
* Assunto
* Data de distribuição (em formato Date)
* Juiz (ou Relator, para processos em 2º Grau)
* Valor da ação (em centavos, para facilitar cálculos e conversões)
* Partes do processo
* Lista das movimentações (Data e descrição do movimento)


### Testes

Para executar os testes é preciso rodar o seguinte comando na raiz do projeto:

```
pipenv install --dev
```

E em cada módulo executar: 

```
make test
```

Os testes de crawleamento foram feitos com a utilização de mocks para os serviços externos. Ambos os módulos possuem testes usando o framework `pytest`.


### Know Issues

* A API poderia ter a adaptação para funcionar em paralelo ao sistema de `Push` do e-SAJ, fazendo com que a cada notificação de atualização de processos seja feita uma nova requisição no sistema para atualizar o objeto salvo. O mesmo pode acontecer para os dados de processos coletados com os Diários Oficiais.

* Em termos de tolerância uma melhoria possível é a implementação de `Dead queues`, para situações em que um sistema responsável está indisponível e não se quer perder o dado do processo criado, no caso, o número. Nesses casos, o número voltaria para uma fila, podendo ser recuperado e solicitado novamente em um momento de revisão ou de disponibilidade confirmada.

* O orquestramento dos componentes do Kafka pode agir ainda mais em favor da escalabilidade. Soluções possíveis envolvem o particionamento customizado, que além de paralelizar o consumo pode "personalizar" a leitura e processamento dos dados, por exemplo, particionando de acordo com o tribunal ao qual pertencem os processos. Além desta, outra melhoria importante é a utilização de múltiplos consumer groups para aumentar a flexibilidade no consumo das mensagens.

* O Worker não faz uso de um framework para processamento em razão da simplicidade, mas é viável a migração para outras soluções como o Celery.
