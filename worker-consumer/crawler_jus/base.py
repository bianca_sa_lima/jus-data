import multitasking
import signal

# kill all tasks on ctrl-c
signal.signal(signal.SIGINT, multitasking.killall)

class AbstractConsumerHandler:
    def __init__(self, consumer):
        self.consumer = consumer

    def handle_value(self, msg):
        pass

    def handle_error(self, msg):
        pass

    def handle_none(self, msg):
        pass

    def stop_criteria(self, msg):
        pass

    @multitasking.task
    def loop_consumer(self):
        while True:
            msg = self.consumer.poll(timeout=1.0)

            if msg is None: self.handle_none(msg)
            elif msg.error(): self.handle_error(msg)
            else: self.handle_value(msg)

            if self.stop_criteria(msg):
                self.consumer.close()
                break

    def run(self):
        self.loop_consumer()