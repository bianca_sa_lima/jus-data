import json
import redis
import logging

from tenacity import retry, stop_after_attempt, wait_fixed

from crawler_jus.base import AbstractConsumerHandler
from crawler_jus import settings

from func_timeout import func_timeout, FunctionTimedOut

logger = logging.getLogger(__name__)

redis_db = redis.Redis(**settings.REDIS_CONFIGURATION)


@retry(stop=stop_after_attempt(5), wait=wait_fixed(2))
def run_crawler_with_retry(crawler, data):
    crawler.run(data)


class NumeroProcessoConsumer(AbstractConsumerHandler):
    def __init__(self, consumer, crawler):
        super(NumeroProcessoConsumer, self).__init__(consumer)
        self.crawler = crawler

    def stop_criteria(self, msg):
        # never ends
        return False
          
    def handle_value(self, msg):
        self.run_crawler_with_lock_and_retry(msg)

    def run_crawler_with_lock_and_retry(self, msg):
        data = msg.value().decode(settings.ENCODING)
        msg_data = json.loads(data)
        with redis_db.lock(data, timeout=settings.REDIS_LOCK_TIMEOUT, blocking_timeout=settings.REDIS_LOCK_BLOCK_TIMEOUT) as lock:
            try:
                func_timeout(settings.MAX_TIME_CRAWLER, run_crawler_with_retry, args=(self.crawler, msg_data))
            except FunctionTimedOut:
                logger.error('It was not possible to run the Crawler within the given timeout.')
            except Exception as e:
                logger.error(f'Exception: {e}')