import os

from dotenv import load_dotenv
load_dotenv()

KAFKA_CONSUMER = {
    'bootstrap.servers': os.environ['CLOUDKARAFKA_BROKERS'],
    'group.id': "%s-consumer" % os.environ['CLOUDKARAFKA_USERNAME'],
    'session.timeout.ms': 6000,
    'default.topic.config': {'auto.offset.reset': 'smallest'},
    'security.protocol': 'SASL_SSL',
    'sasl.mechanisms': 'SCRAM-SHA-256',
    'sasl.username': os.environ['CLOUDKARAFKA_USERNAME'],
    'sasl.password': os.environ['CLOUDKARAFKA_PASSWORD']
}

KAFKA_TOPIC_PREFIX = os.environ["CLOUDKARAFKA_TOPIC_PREFIX"]

ENCODING = "utf-8"

EXTERNAL_SERVICE_TIMEOUT=int(os.environ["EXTERNAL_SERVICE_TIMEOUT"])

PROCESSOS_API = os.environ["PROCESSOS_API"] + '/processos/{}/'

REDIS_CONFIGURATION = {
    'password': os.environ["REDIS_PASSWORD"], 
    "host": os.environ["REDIS_HOST"], 
    "port": 12718, 
    "db": 0
}

REDIS_LOCK_TIMEOUT=int(os.environ["REDIS_LOCK_TIMEOUT"])
REDIS_LOCK_BLOCK_TIMEOUT=int(os.environ["REDIS_LOCK_BLOCK_TIMEOUT"])
MAX_TIME_CRAWLER=int(os.environ["MAX_TIME_CRAWLER"])

TJAL_CODE = '02'
TJMS_CODE = '12'