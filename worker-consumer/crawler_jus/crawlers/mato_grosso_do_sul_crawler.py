from crawler_jus.crawlers.base_crawler import AbstractCrawler
from crawler_jus import settings

import requests
import re
import json
from datetime import datetime
from dateutil.parser import parse
from bs4 import BeautifulSoup


MAIN_DATA_FIRST_DEGREE = ['Assunto', 'Classe', 'Juiz']
DETAILS_FIRST_DEGREE = ['Área', 'Distribuição', 'Valor da Ação']

MAIN_DATA_SECOND_DEGREE = ['Área', 'Assunto', 'Classe']
DETAILS_SECOND_DEGREE = ['Relator', 'Valor da Ação']

def get_process_from_TJMS(process_number, grau=1):
    url = 'https://esaj.tjms.jus.br/cpopg5/search.do'
    params = {
        'cbPesquisa': 'NUMPROC',
        'numeroDigitoAnoUnificado': process_number[:15],
        'foroNumeroUnificado': process_number[-4:],
    }

    if grau == 1:
        params_primeiro_grau = {
            'dadosConsulta.tipoNuProcesso': 'UNIFICADO',
            'dadosConsulta.valorConsultaNuUnificado': process_number,
        }
        params = {**params, **params_primeiro_grau}
    elif grau == 2:
        url = 'https://esaj.tjms.jus.br/cposg5/search.do'
        params_segundo_grau = {
            'tipoNuProcesso': 'UNIFICADO',
            'dePesquisaNuUnificado': process_number
        }
        params = {**params, **params_segundo_grau}
        
    response = get_page(url, params)

    return response.content

def get_page(url, params):
    session = requests.session()
    return session.get(url, params=params, timeout=settings.EXTERNAL_SERVICE_TIMEOUT)

def pre_process(soup):
    tags = {}
    dados_principais_div_list = soup.find("div", {"id": "containerDadosPrincipaisProcesso"})
    tags['DIV_DADOS_PRINCIPAIS'] = dados_principais_div_list
    dados_div_row = soup.findAll("div", {"class": "row"})
    tags['DIV_ROWS'] = dados_div_row
    mais_detalhes_div_list = soup.find("div", {"id": "maisDetalhes"})
    tags['DIV_MAIS_DETALHES'] = mais_detalhes_div_list
    todas_movimentacoes_tr_list = soup.select("#tabelaTodasMovimentacoes tr")
    ultimas_movimentacoes_tr_list = soup.select("#tabelaUltimasMovimentacoes tr")
    tags['TR_TODAS_MOVIMENTACOES'] = []
    tags['TR_TODAS_MOVIMENTACOES'].append(todas_movimentacoes_tr_list)
    tags['TR_TODAS_MOVIMENTACOES'].append(ultimas_movimentacoes_tr_list)
    todas_partes_tr_list = soup.select("#tableTodasPartes tr")
    partes_principais_tr_list = soup.select("#tablePartesPrincipais tr")
    tags['TR_TODAS_PARTES'] = []
    tags['TR_TODAS_PARTES'].append(todas_partes_tr_list)
    tags['TR_TODAS_PARTES'].append(partes_principais_tr_list)
    
    return tags

def get_main_data(name, tr_tags, grau=1):
    tag_list = []
    empty = ''
    if grau == 1: 
        tag_list = tr_tags['DIV_DADOS_PRINCIPAIS'] 
    elif grau == 2: 
        tag_list = tr_tags['DIV_ROWS']
    if tag_list != None: 
        for div in tag_list:
            for sub_div in div:
                div_span = sub_div.find('span') if sub_div != None else ''
                span_title = div_span.text if type(div_span) != int and div_span != None else ''
                if span_title.strip() == name:
                    field_value = sub_div.find('div') if sub_div != None else ''
                    return field_value.text.strip()
    return empty

def get_detail_data(name, tr_tags, grau=1):
    div_list = tr_tags['DIV_MAIS_DETALHES']
    empty = ''
    if div_list != None: 
        for div in div_list:
            if div != None:
                for sub_div in div:
                    if name == 'Juiz' and grau == 2:
                        name = 'Relator'
                    div_span = sub_div.find('span') if sub_div != None else ''
                    span_title = div_span.text if type(div_span) != int else ''
                    if span_title.strip() == name:
                        field_value = sub_div.find('div') if sub_div != None else ''
                        return field_value.text.strip()
    return empty

def get_data(name, tr_tags, grau=1):
    if grau == 1:
        if name in MAIN_DATA_FIRST_DEGREE:
            return get_main_data(name, tr_tags, grau)
        else:
            return get_detail_data(name, tr_tags, grau)
    elif grau == 2:
        if name in MAIN_DATA_SECOND_DEGREE:
            return get_main_data(name, tr_tags, grau)
        else:
            return get_detail_data(name, tr_tags, grau)

    return ''

def process_detached_data(tr_tags, grau=1):
    process_data = {}
    process_data['classe'] = get_data('Classe', tr_tags, grau)
    process_data['assunto'] = get_data('Assunto', tr_tags, grau)
    process_data['nome_juiz_relator'] = get_data('Juiz', tr_tags, grau)
    process_data['area'] = get_data('Área', tr_tags, grau)
    distribuition = get_data('Distribuição', tr_tags, grau)
    process_data['data_distribuicao'] = string_to_brdate(distribuition)
    action_value = get_data('Valor da ação', tr_tags, grau)
    process_data['valor_acao'] = string_to_int(action_value)
    return process_data

def process_movements(tr_tags):
    movements = []
    for tr in tr_tags['TR_TODAS_MOVIMENTACOES']:
        if tr != None:
            for element in tr:
                td = element.select('td')
                moviment = {}
                moviment_date = string_to_brdate(td[0].text.strip())
                description = td[2].text.strip()
                if not contains_movements(moviment_date, description, movements):
                    moviment['data_movimentacao'] = moviment_date
                    moviment['descricao'] = description
                    movements.append(moviment)
    return movements

def contains_movements(moviment_date, description, list_of_movements):
    for moviment in list_of_movements:
        if moviment['data_movimentacao'] == moviment_date and moviment['descricao'] == description:
            return True
    return False

def process_parts(tr_tags):
    parts = []
    for tr in tr_tags['TR_TODAS_PARTES']:
        part = []
        if tr != None:
            for td_tag in tr:
                td = td_tag.select('td')
                if td != None and td != '': 
                    main_part = {}
                    part_class = td[0].text.strip()
                    nome_envolvido = td[1].contents[0].strip()
                    if not contains_part(nome_envolvido, parts):
                        main_part['tipo_envolvimento'] = part_class.split(':')[0]
                        main_part['nome_envolvido'] = nome_envolvido
                        main_part['advogados'] = []

                        lawyers_class = td[1].select('span')

                        for lawyer in lawyers_class:
                            lawyer_part = {}
                            lawyer_title = lawyer.text.strip()[:-1]
                            lawyer_part['tipo_advogado'] = lawyer_title
                            nome_envolvido_advogado = lawyer.next_sibling.strip()
                            lawyer_part['nome'] = nome_envolvido_advogado
                            main_part['advogados'].append(lawyer_part)
                        
                        parts.append(main_part)

    return parts

def contains_part(part_name, list_of_parts):
    for part in list_of_parts:
        if part['nome_envolvido'] == part_name:
            return True
    return False


def string_to_date(date):
    return datetime.strptime(date, '%d/%m/%Y').date()

def date_to_string(date):
    return datetime.strftime(date, '%Y-%m-%d')

def string_to_brdate(date):
    raw_date = date
    if date != None and date != '' and is_date(raw_date.split(' ')[0]):
        date = raw_date.split(' ')[0]
        date = string_to_date(date)
        return date_to_string(date)
    elif date != None and date != '' and not is_date(raw_date.split(' ')[0]): 
        return None
    else:
        return None

def is_date(string, fuzzy=False):
    try: 
        parse(string, fuzzy=fuzzy)
        return True
    except ValueError:
        return False

def string_to_int(action_value):
    int_value = 0
    if action_value != None and len(action_value.split()) > 0:
        str_value = action_value.split()[-1]
        value = re.split(r'[.,]', str_value)
        int_value = int("".join(value)) 
    return int_value

class MatoGrossoDoSulCrawler(AbstractCrawler):
    def process(process_number, instance):
        page_content = get_process_from_TJMS(process_number, instance)
        soup = BeautifulSoup(page_content, "html.parser")
        tr_tags = pre_process(soup)
        process_data = process_detached_data(tr_tags, instance)
        movements = process_movements(tr_tags)
        parts = process_parts(tr_tags)
        process_data['partes'] = parts
        process_data['movimentacoes'] = movements

        return process_data