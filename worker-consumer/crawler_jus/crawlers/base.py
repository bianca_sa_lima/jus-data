import re
import json
import requests
import logging
from crawler_jus import settings

from crawler_jus.crawlers.alagoas_crawler import AlagoasCrawler
from crawler_jus.crawlers.mato_grosso_do_sul_crawler import MatoGrossoDoSulCrawler


logger = logging.getLogger(__name__)


CRAWLER_BY_COURT = {
  settings.TJAL_CODE: AlagoasCrawler,
  settings.TJMS_CODE: MatoGrossoDoSulCrawler
}


def get_court(process_number):
  return re.split(r'[-.]', process_number)[4]


def requisita_tribunal(numero_process, instance):
  court = get_court(numero_process)

  if court in CRAWLER_BY_COURT:
    return CRAWLER_BY_COURT[court].process(numero_process, instance)

  logger.info(f"Esse processo percente a um tribunal que o crawler não reconhece {numero_process}")

  return None


def patch_process(process_data, id, instance=1):
  payload = {}
  headers = {
    'Content-Type': 'application/json'
  }
  instance_data = "dados_processo_primeiro_grau" if instance == 1 else "dados_processo_segundo_grau"

  jus_data = requisita_tribunal(process_data, instance)
  payload[instance_data] = jus_data

  return requests.request("PATCH", url=settings.PROCESSOS_API.format(id),
    headers=headers, data=json.dumps(payload), timeout=settings.EXTERNAL_SERVICE_TIMEOUT)


def run(data):
  value = data['process_number']
  
  id = data['id']
  response = requests.get(settings.PROCESSOS_API.format(id), timeout=settings.EXTERNAL_SERVICE_TIMEOUT)

  if response.status_code != 200:
    logger.info(f"Falha ao consultar processo com id> {id}. Devido: {response.text}")
    raise Exception(f"GET processo com id {id} falhou.")

  for instance in [1, 2]:
    response_patch = patch_process(value, id, instance)

    if response_patch.status_code != 200:
      logger.info(f"Falha ao atualizar processo {value} no grau {instance}. Devido: {response_patch.text}")

  logger.info(f"id: {id}, processo: {value}")