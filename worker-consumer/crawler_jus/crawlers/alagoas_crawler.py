from crawler_jus.crawlers.base_crawler import AbstractCrawler
from crawler_jus import settings

import requests
import re
import json
from datetime import datetime
from dateutil.parser import parse
from bs4 import BeautifulSoup


def get_process_from_TJAL(process_number, grau=1):
    url = 'https://www2.tjal.jus.br/cpopg/search.do'
    params = {
        'cbPesquisa': 'NUMPROC',
        'numeroDigitoAnoUnificado': process_number[:15],
        'foroNumeroUnificado': process_number[-4:],
    }

    if grau == 1:
        params_primeiro_grau = {
            'dadosConsulta.tipoNuProcesso': 'UNIFICADO',
            'dadosConsulta.valorConsultaNuUnificado': process_number,
        }
        params = {**params, **params_primeiro_grau}
    elif grau == 2:
        url = 'https://www2.tjal.jus.br/cposg5/search.do?'
        params_segundo_grau = {
            'tipoNuProcesso': 'UNIFICADO',
            'dePesquisaNuUnificado': process_number,
            'pbEnviar': 'Pesquisar'
        }
        params = {**params, **params_segundo_grau}
        
    response = get_page(url, params)

    return response.content

def get_page(url, params):
    session = requests.session()
    return session.get(url, params=params, timeout=settings.EXTERNAL_SERVICE_TIMEOUT)

def pre_process(soup):
    tags = {}
    secao_form_body_tr_list = soup.select("table.secaoFormBody tr")
    tags['TR_SECAO_FORM_BODY'] = secao_form_body_tr_list
    todas_movimentacoes_tr_list = soup.select("#tabelaTodasMovimentacoes tr")
    tags['TR_TODAS_MOVIMENTACOES'] = todas_movimentacoes_tr_list
    todas_partes_tr_list = soup.select("#tableTodasPartes tr")
    partes_principais_tr_list = soup.select("#tablePartesPrincipais tr")
    tags['TR_TODAS_PARTES'] = []
    tags['TR_TODAS_PARTES'].append(todas_partes_tr_list)
    tags['TR_TODAS_PARTES'].append(partes_principais_tr_list)
    return tags

def get_data(name, tr_tags, grau=1):
    tr_list = tr_tags['TR_SECAO_FORM_BODY']
    empty = ''
    for tr in tr_list:
        td = tr.find('td')
        if td != None:
            field = td.text.strip()[:-1]
            if name == 'Juiz' and grau == 2:
                name = 'Relator'
            if field == name and name != 'Área':
                value = td.find_next_sibling('td').text.strip() if td.find_next_sibling('td') != None else ''
                return value
            if name == 'Área':
                if grau == 1:
                    area_field = td.find('span')
                    area_field_text = area_field.text[:-1] if area_field != None else ''
                    if area_field != None and area_field_text == name:
                        if area_field.next_element != None and area_field.next_element.next_element != None:
                            return area_field.next_element.next_element.strip()
                if grau == 2:
                    area_field = td.text.split(':')[0].strip() if td != None else ''
                    next_field = td.find_next_sibling('td')
                    field_value = next_field.text.strip() if next_field != None else ''
                    if area_field == name:
                        return field_value
    return empty

def process_detached_data(tr_tags, grau=1):
    process_data = {}
    process_data['classe'] = get_data('Classe', tr_tags)
    process_data['area'] = get_data('Área', tr_tags, grau)
    process_data['assunto'] = get_data('Assunto', tr_tags)
    distribuition = get_data('Distribuição', tr_tags)
    process_data['data_distribuicao'] = string_to_brdate(distribuition)
    process_data['nome_juiz_relator'] = get_data('Juiz', tr_tags, grau)
    action_value = get_data('Valor da ação', tr_tags)
    process_data['valor_acao'] = string_to_int(action_value)
    return process_data

def process_moviments(tr_tags):
    moviments = []
    for element in tr_tags['TR_TODAS_MOVIMENTACOES']:
        td = element.select('td')
        moviment = {}
        moviment['data_movimentacao'] = string_to_brdate(td[0].text.strip())
        moviment['descricao'] = td[2].text.strip()
        moviments.append(moviment)
    return moviments

def process_parts(tr_tags):
    parts = []
    for tr in tr_tags['TR_TODAS_PARTES']:
        part = []
        for td_tag in tr:
            td = td_tag.select('td')
            if td != None and td != '': 
                main_part = {}
                part_class = td[0].text.strip()[:-1]
                nome_envolvido = td[1].contents[0].strip()
                if not contains_part(nome_envolvido, parts):
                    main_part['tipo_envolvimento'] = part_class
                    main_part['nome_envolvido'] = nome_envolvido
                    main_part['advogados'] = []

                    lawyers_class = td[1].select('span')

                    for lawyer in lawyers_class:
                        lawyer_part = {}
                        lawyer_title = lawyer.text.strip()[:-1]
                        lawyer_part['tipo_advogado'] = lawyer_title
                        nome_envolvido_advogado = lawyer.next_sibling.strip()
                        lawyer_part['nome'] = nome_envolvido_advogado
                        main_part['advogados'].append(lawyer_part)
                    
                    parts.append(main_part)

    return parts

def contains_part(part_name, list_of_parts):
    for part in list_of_parts:
        if part['nome_envolvido'] == part_name:
            return True
    return False

def string_to_date(date):
    return datetime.strptime(date, '%d/%m/%Y').date()

def date_to_string(date):
    return datetime.strftime(date, '%Y-%m-%d')

def string_to_brdate(date):
    raw_date = date
    if date != None and date != '' and is_date(raw_date.split(' ')[0]):
        date = raw_date.split(' ')[0]
        date = string_to_date(date)
        return date_to_string(date)
    elif date != None and date != '' and not is_date(raw_date.split(' ')[0]): 
        return None
    else:
        return None

def is_date(string, fuzzy=False):
    try: 
        parse(string, fuzzy=fuzzy)
        return True
    except ValueError:
        return False

def string_to_int(action_value):
    int_value = 0
    if action_value != None and len(action_value.split()) > 0:
        str_value = action_value.split()[-1]
        value = re.split(r'[.,]', str_value)
        int_value = int("".join(value)) 
    return int_value

class AlagoasCrawler(AbstractCrawler):
    def process(process_number, instance):
        page_content = get_process_from_TJAL(process_number, instance)
        soup = BeautifulSoup(page_content, "html.parser")
        tr_tags = pre_process(soup)
        process_data = process_detached_data(tr_tags, instance)
        moviments = process_moviments(tr_tags)
        parts = process_parts(tr_tags)
        process_data['partes'] = parts
        process_data['movimentacoes'] = moviments

        return process_data