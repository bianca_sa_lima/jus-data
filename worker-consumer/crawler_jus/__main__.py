from confluent_kafka import Consumer
import logging

from crawler_jus import settings
from crawler_jus.crawlers import base as crawler_base

from crawler_jus.consumer import NumeroProcessoConsumer


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    TOPIC = settings.KAFKA_TOPIC_PREFIX + "justest"
    consumer = Consumer(**settings.KAFKA_CONSUMER)
    consumer.subscribe([TOPIC])

    numero_processo_consumer = NumeroProcessoConsumer(consumer, crawler_base)

    numero_processo_consumer.run()





























