import mock

from tests import FIXTURE_DIRS
from crawler_jus.crawlers.alagoas_crawler import AlagoasCrawler
from crawler_jus.crawlers.mato_grosso_do_sul_crawler import MatoGrossoDoSulCrawler

@mock.patch('crawler_jus.crawlers.alagoas_crawler.get_process_from_TJAL')
def test_alagoas_crawler_grau_1(mock_process_content):
  content = open(FIXTURE_DIRS + '/0708956-13.2012.8.02.0001__01.html', 'r').read()

  mock_process_content.return_value = content

  processo = "0708956-13.2012.8.02.0001"
  data = AlagoasCrawler.process(processo, 1)
  
  expected_data = {"classe": "Exibição"}

  for key in expected_data:
    assert data[key] == expected_data[key]

@mock.patch('crawler_jus.crawlers.alagoas_crawler.get_process_from_TJAL')
def test_alagoas_crawler_1_2(mock_process_content):
  content = open(FIXTURE_DIRS + '/0500066-96.2017.8.02.0000__01.html', 'r').read()
  mock_process_content.return_value = content

  processo = "0708956-13.2012.8.02.0001"
  data = AlagoasCrawler.process(processo, 1)
  expected_data = {"nome_juiz_relator": ""}

  for key in expected_data:
    assert data[key] == expected_data[key]

  content = open(FIXTURE_DIRS + '/0500066-96.2017.8.02.0000__02.html', 'r').read()
  mock_process_content.return_value = content

  data = AlagoasCrawler.process(processo, 2)
  expected_data = {"nome_juiz_relator": "DES. SEBASTIÃO COSTA FILHO"}

  for key in expected_data:
    print (key)
    assert data[key] == expected_data[key]

#0800145-87.2013.8.12.0024
@mock.patch('crawler_jus.crawlers.mato_grosso_do_sul_crawler.get_process_from_TJMS')
def test_mato_grosso_do_sul_crawler_1_2(mock_process_content):
  content = open(FIXTURE_DIRS + '/0800145-87.2013.8.12.0024__01.html', 'r').read()
  mock_process_content.return_value = content

  processo = "0800145-87.2013.8.12.0024"
  data = MatoGrossoDoSulCrawler.process(processo, 1)
  expected_data = {"valor_acao": 4294599}

  for key in expected_data:
    assert data[key] == expected_data[key]

  content = open(FIXTURE_DIRS + '/0800145-87.2013.8.12.0024__02.html', 'r').read()
  mock_process_content.return_value = content

  data = MatoGrossoDoSulCrawler.process(processo, 2)
  expected_data = {"valor_acao": 4294599}

  for key in expected_data:
    print (key)
    assert data[key] == expected_data[key]


#0800145-87.2013.8.12.0024
@mock.patch('crawler_jus.crawlers.mato_grosso_do_sul_crawler.get_process_from_TJMS')
def test_mato_grosso_do_sul_crawler_2_grau(mock_process_content):
  content = open(FIXTURE_DIRS + '/0800145-87.2013.8.12.0024__02.html', 'r').read()
  mock_process_content.return_value = content
  processo = "0800145-87.2013.8.12.0024"

  data = MatoGrossoDoSulCrawler.process(processo, 2)
  expected_data = {
    "partes": [
      {
        "advogados": [
          {
            "nome": "James Robert Silva",
            "tipo_advogado": "Advogado"
          },
          {
            "nome": "Carlos Humberto Batalha",
            "tipo_advogado": "Advogado"
          }
        ],
        "nome_envolvido": "Guemes César de Freitas",
        "tipo_envolvimento": "Apelante"
      },
      {
        "advogados": [
          {
              "nome": "Adriana Maria de Castro Rodrigues",
              "tipo_advogado": "Procuradora"
          }
        ],
        "nome_envolvido": "Instituto Brasileiro do Meio Ambiente e dos Recursos Naturais Renováveis - Ibama",
        "tipo_envolvimento": "Apelado"
      }
    ]
  }

  for key in expected_data:
    print (key)
    assert data[key] == expected_data[key]