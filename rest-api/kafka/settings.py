import os
from dotenv import load_dotenv

# required to load envvars from .env file to export at the os
load_dotenv()

KAFKA_PRODUCER = {
    'bootstrap.servers': os.environ['CLOUDKARAFKA_BROKERS'],
    'session.timeout.ms': 6000,
    'default.topic.config': {'auto.offset.reset': 'smallest'},
    'security.protocol': 'SASL_SSL',
    'sasl.mechanisms': 'SCRAM-SHA-256',
    'sasl.username': os.environ['CLOUDKARAFKA_USERNAME'],
    'sasl.password': os.environ['CLOUDKARAFKA_PASSWORD']
}

KAFKA_TOPIC_PREFIX = os.environ["CLOUDKARAFKA_TOPIC_PREFIX"]

ENCODING = "utf-8"