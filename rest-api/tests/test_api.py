import pytest
import json
import logging

from django.test import Client
from rest_framework.reverse import reverse

from processos_api.models import Processo

client = Client()

logger = logging.getLogger(__name__)

@pytest.mark.django_db
def test_get_processes():
  response = client.get(reverse('processo-list'))

  assert response.status_code == 200
  data = response.data

  expected = {"count": 0, "next": None, "previous": None, "results": []}
  assert json.dumps(data) == json.dumps(expected)


@pytest.mark.django_db
def test_create_valid_process():
      
  valid_payload = {
    "numero_processo": "0894670-56.2019.8.02.0000"
  }
  response = client.post(
    reverse('processo-list'),
    data=json.dumps(valid_payload),
    content_type='application/json')
  assert response.status_code == 201

@pytest.mark.django_db
def test_create_invalid_process():
      
  valid_payload = {
    "numero_processo": "0894670-56.2019.8.02.00000"
  }
  response = client.post(
    reverse('processo-list'),
    data=json.dumps(valid_payload),
    content_type='application/json')
  assert response.status_code == 400
