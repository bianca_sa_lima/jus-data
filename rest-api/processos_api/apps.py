from django.apps import AppConfig


class ProcessosApiConfig(AppConfig):
    name = 'processos_api'
