import json
from confluent_kafka import Producer
from tenacity import retry, stop_after_attempt, wait_fixed

import logging

from kafka import settings

TOPIC = settings.KAFKA_TOPIC_PREFIX + "justest"

producer = Producer(**settings.KAFKA_PRODUCER)
logger = logging.getLogger(__name__)


@retry(stop=stop_after_attempt(5), wait=wait_fixed(2))
def new_process(process):
    message = {'id': str(process.id),
            'process_number': process.numero_processo}
    send_message(json.dumps(message))


def delivery_callback(err, msg):
    if err:
        logger.error('%% Message failed delivery: %s\n' % err)
    else:
        logger.info('%% Message delivered to %s [%d]\n' %
                            (msg.topic(), msg.partition()))


def send_message(message):
    producer.produce(TOPIC, message, callback=delivery_callback)
    producer.poll(0)
    logger.info('Message sent to Kafka')
    producer.flush()