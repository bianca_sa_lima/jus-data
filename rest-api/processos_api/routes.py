from rest_framework import routers

from processos_api.views import ProcessoViewSet

router = routers.DefaultRouter()
router.register(r'processos', ProcessoViewSet)