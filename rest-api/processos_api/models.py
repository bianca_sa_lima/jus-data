import uuid
from django.db import models

from processos_api import hooks

from simple_history.models import HistoricalRecords
from django_lifecycle import LifecycleModel, hook, AFTER_CREATE

class DadosProcesso(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

  assunto = models.CharField(max_length=4_000, default="", null=True, blank=True)
  classe = models.CharField(max_length=200, default="", null=True, blank=True)
  area = models.CharField(max_length=200, default="", null=True, blank=True)
  data_distribuicao = models.DateField(null=True, blank=True)
  nome_juiz_relator = models.CharField(max_length=200, default="", null=True, blank=True)
  valor_acao = models.IntegerField(default=0, null=True, blank=True)

  created_at = models.DateTimeField(auto_now_add=True, db_index=True)
  updated_at = models.DateTimeField(auto_now=True)

  history = HistoricalRecords()

  def __str__(self):
    return f"assunto: {self.assunto} na classe: {self.classe} na área: {self.area}."

  class Meta:
        verbose_name_plural = "Dados dos processo"


class Movimentacao(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

  data_movimentacao = models.DateField(null=False)
  descricao = models.CharField(max_length=8_000, default="")

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  processo = models.ForeignKey("DadosProcesso", related_name="movimentacoes", on_delete=models.CASCADE)

  history = HistoricalRecords()

  def __str__(self):
    return f"descricao {self.descricao}"

  class Meta:
        verbose_name_plural = "Movimentações"


class Advogado(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

  nome = models.CharField(max_length=200, help_text="Nome da pessoa que ira advogar")
  tipo_advogado  = models.CharField(max_length=200, default='')

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  parte = models.ForeignKey("Parte", related_name="advogados", on_delete=models.CASCADE)

  history = HistoricalRecords()

  class Meta:
        verbose_name_plural = "Advogados/as das partes"

  def __str__(self):
    return f"Nome: {self.nome}"


class Parte(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

  nome_envolvido = models.CharField(max_length=200, null=False, default="", help_text="Nome da pessoa parte do processo")
  tipo_envolvimento = models.CharField(max_length=200, default='', help_text="Tipo do envolvimento no processo")

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  processo = models.ForeignKey("DadosProcesso", related_name="partes", on_delete=models.CASCADE)

  history = HistoricalRecords()

  def __str__(self):
    return f"Nome: {self.nome_envolvido}, tipo: {self.tipo_envolvimento}"

  class Meta:
        verbose_name_plural = "Partes/Envolvidos do Processo"

class Processo(LifecycleModel, models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

  numero_processo = models.CharField(max_length=30, null=False, db_index=True, help_text="Numero do processo", unique=True)

  created_at = models.DateTimeField(auto_now_add=True, db_index=True)
  updated_at = models.DateTimeField(auto_now=True)

  dados_processo_primeiro_grau = models.OneToOneField(DadosProcesso, on_delete=models.CASCADE, related_name="primeiro_grau", null=True)
  dados_processo_segundo_grau = models.OneToOneField(DadosProcesso, on_delete=models.CASCADE, related_name="segundo_grau", null=True)

  history = HistoricalRecords()

  def __str__(self):
    return f"Processo de número {self.numero_processo}"

  class Meta:
        verbose_name_plural = "Processos"
        ordering = ["-created_at"]

  @hook(AFTER_CREATE)
  def after_creation(self):
    hooks.new_process(self)