from django_filters import rest_framework as filters
from rest_framework import viewsets

from processos_api.serializers import ProcessoSerializer
from processos_api.models import Processo

class ProductFilter(filters.FilterSet):
    class Meta:
        model = Processo
        fields = ('numero_processo',)


class ProcessoViewSet(viewsets.ModelViewSet):
    queryset = Processo.objects.all()
    serializer_class = ProcessoSerializer

    http_method_names = ['get', 'post', 'head', 'put', 'patch', 'delete']

    ordered = ('-created_at',)

    filterset_class = ProductFilter
    filter_backends = (filters.DjangoFilterBackend,)
