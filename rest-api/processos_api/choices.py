TIPOS_ADVOGADOS = (("AO", "Advogado"), ("AA", "Advogada"))
DEFAULT_TIPO_ADVOGADO =TIPOS_ADVOGADOS[0][0]

TIPOS_ENVOLVIMENTOS = (("RU", "Réu"), ("RE", "Ré"),
                    ("TI", "Terceiro interessado"), 
                    ("AT", "Autor"), ("ATA", "Autora"),
                    ("APE", "Apelante"), ("APO", "Apelado"),
                    ("REP", "Representante"), ("RPA", "Representada"), ("RPO", "Representado"))
DEFAULT_TIPO_ENVOLVIMENTO=TIPOS_ENVOLVIMENTOS[0][0]