import re

from rest_framework import serializers

from drf_writable_nested.serializers import WritableNestedModelSerializer

from processos_api import models

class AdvogadoSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = models.Advogado
    fields = ('id', 'nome', 'tipo_advogado', 'created_at', 'updated_at')


class ParteSerializer(WritableNestedModelSerializer, serializers.HyperlinkedModelSerializer):
  advogados = AdvogadoSerializer(many=True, allow_null=True, required=False)

  class Meta:
    model = models.Parte
    fields = ['id', 'nome_envolvido', 'tipo_envolvimento', 'advogados', 'created_at', 'updated_at']


class MovimentacaoSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = models.Movimentacao
    fields = ['id', 'data_movimentacao', 'descricao', 'created_at', 'updated_at']


class DadosProcessoSerializer(WritableNestedModelSerializer, serializers.HyperlinkedModelSerializer):
  movimentacoes = MovimentacaoSerializer(many=True, allow_null=True, required=False)
  partes = ParteSerializer(many=True, allow_null=True, required=False)

  class Meta:
    model = models.DadosProcesso
    fields = ['id', 'assunto', 'classe', 'area', 'data_distribuicao', 'nome_juiz_relator', 'valor_acao', 'created_at', 'updated_at', 'movimentacoes', 'partes']
    read_only_fields = ('created_at', 'updated_at')


class ProcessoSerializer(WritableNestedModelSerializer, serializers.HyperlinkedModelSerializer):
    dados_processo_primeiro_grau= DadosProcessoSerializer(allow_null=True, required=False)
    dados_processo_segundo_grau = DadosProcessoSerializer(allow_null=True, required=False)

    class Meta:
        model = models.Processo
        fields = ['id', 'numero_processo', 'dados_processo_primeiro_grau', 'dados_processo_segundo_grau', 'created_at', 'updated_at']
        read_only_fields = ('created_at', 'updated_at')

    def validate_numero_processo(self, numero_processo):
      if not re.match(r'\d{7}\-\d{2}\.\d{4}\.\d\.\d{2}\.\d{4}$', numero_processo):
        raise serializers.ValidationError("Número de processo com formatação inválida.")
      return numero_processo