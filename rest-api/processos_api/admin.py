from django.contrib import admin

from processos_api import models

admin.site.register(models.Processo)
admin.site.register(models.DadosProcesso)
admin.site.register(models.Parte)
admin.site.register(models.Advogado)
admin.site.register(models.Movimentacao)